package com.example.tp1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Second_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_);
        // get the text from MainActivity
        String text = getIntent().getStringExtra("val");

       final TextView t1 = (TextView)findViewById(R.id.CountryNameSecond);
        t1.setText(text);


        Country object = CountryList.getCountry(text);
        ImageView imagex=(ImageView)findViewById(R.id.imageViewSecond);
        String urlImage=getIntent().getStringExtra("imageUrl");
        int id = getResources().getIdentifier("com.example.tp1:drawable/" + urlImage, null, null);
        imagex.setImageResource(id);

        final EditText e1 = (EditText)findViewById(R.id.CapitaleEditText);
        final EditText e2 = (EditText)findViewById(R.id.LangueOfficielEditText);
        final EditText e3 = (EditText)findViewById(R.id.MonnaieEditText);
        final EditText e4 = (EditText)findViewById(R.id.PopulationEditText);
        final EditText e5 = (EditText)findViewById(R.id.SuperficieEditText);
        e1.setText(getIntent().getStringExtra("capitale"));
        e2.setText(getIntent().getStringExtra("langue"));
        e3.setText(getIntent().getStringExtra("monnaie"));

        e4.setText(Integer.toString(getIntent().getIntExtra("population",0)));
        e5.setText(Integer.toString(getIntent().getIntExtra("superficie",0)));
        Button b1 = (Button)findViewById(R.id.ButtonSauvg);



        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                e1.setText(e1.getText());
                e2.setText(e2.getText());
                e3.setText(e3.getText());
                e4.setText(e4.getText());
                e5.setText(e5.getText());
               Country object = CountryList.getCountry(String.valueOf(t1.getText()));
               object.setmCapital(String.valueOf(e1.getText()));
                object.setmLanguage(String.valueOf(e2.getText()));
                object.setmCurrency(String.valueOf(e3.getText()));
                object.setmPopulation((Integer.parseInt(String.valueOf(e4.getText()))));
                object.setmArea((Integer.parseInt(String.valueOf(e5.getText()))));
            }
        });



    }
}
