package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
    //Adapter qui vas recevoir toute les informations
    MyRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // mon recyclerView vas contenir donc les informations concerner
        // set up le  RecyclerView
        RecyclerView recyclerView = findViewById(R.id.Countries);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        adapter = new MyRecyclerViewAdapter(this, Arrays.asList(CountryList.getNameArray()));
        //Faire un click listener
        adapter.setClickListener(this);
        //mêttre l'adapteur qui vas contenir les informations pour les afficher dans ma vue
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onItemClick(View view, int position) {
        String value = (String) adapter.getItem(position);
        Country country = CountryList.getCountry(value);
        String image = country.getmImgFile();
        String capitale= country.getmCapital();
        String langue=country.getmLanguage();
        String monnaie = country.getmCurrency();
        int population = country.getmPopulation();
        int superficie = country.getmArea();
        Intent myintent = new Intent(view.getContext(), Second_Activity.class);
        myintent.putExtra("val",value);
        myintent.putExtra("imageUrl",image);
        myintent.putExtra("capitale",capitale);
        myintent.putExtra("langue",langue);
        myintent.putExtra("monnaie",monnaie);
        myintent.putExtra("population",population);
        myintent.putExtra("superficie",superficie);
        startActivity(myintent);
        //envoyer les informations vers la deuxieme vue

    }
}