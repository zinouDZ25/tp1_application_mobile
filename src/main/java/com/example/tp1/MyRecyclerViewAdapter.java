package com.example.tp1;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.IconCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;



public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<String> mData;
         private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context, List<String> data) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = data;
         }

        // inflates the row layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.recycler_view_item, parent, false);

            return new ViewHolder(view);
        }

        // binds the data to the TextView in each row
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            String country = mData.get(position);
            holder.myTextView.setText(country);
            Country object = CountryList.getCountry(country);
            int [] countryId ={
                   R.drawable.flag_of_france,
                R.drawable.flag_of_south_africa,
             R.drawable.flag_of_japan,
            R.drawable.flag_of_the_united_states,
            R.drawable.flag_of_spain,
            R.drawable.flag_of_germany
            };
            ArrayList<Integer> mFlags = null;

            holder.myImageView2.setImageResource(countryId[position]);
                     }

        // total number of rows
        @Override
        public int getItemCount() {
            return mData.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView myTextView;
            ImageView myImageView2;
            ViewHolder(View itemView) {

                super(itemView);

                myTextView = itemView.findViewById(R.id.Country);
                myImageView2= (ImageView)itemView.findViewById(R.id.flagD);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                 mClickListener.onItemClick(view, getAdapterPosition());

            }
        }

        // convenience method for getting data at click position
        String getItem(int id) {
            return mData.get(id);
        }
         // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }
    }